package n_queen;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

public class State {
    boolean[][] board;
    boolean[][] boardR;
    int[][] boardI;
    int[][] hBoard;
    int min = Integer.MAX_VALUE;
    int hn = 0;
    int[] pos = new int[2];
    int[] array = new int[]{1,6,5,3,7,7,7,5};

    public State(int size)
    {
        board = new boolean[size][size];
        hBoard = new int[size][size];
        boardI = new int[size][size];
        int k = 0; Random r = new Random();
        for ( int i = 0; i < size; i++ )
        {
            array[i] = r.nextInt(size);
            for ( int j = 0; j < size; j++ )
            {
                if ( j == array[k] )
                {
                    board[j][i] = true;
                    k++;
                    break;
                }
            }
        }
    }
    public State( int[] parentState )
    {
        int size = parentState.length;
        this.array = parentState.clone();
        board = new boolean[size][size];
        hBoard = new int[size][size];
        boardI = new int[size][size];
        int k = 0; Random r = new Random();
        for ( int i = 0; i < size; i++ )
        {
             for ( int j = 0; j < size; j++ )
            {
                if ( j == array[k] )
                {
                    board[j][i] = true;
                    k++;
                    break;
                }
            }
        }
    }
    public void mark(int i, int j)
    {
        boardR[i][j] = true;
        int m = i, n = j;
        while ( m < board.length && n < board.length )
        {
            board[m][n] = true;
            m++; n++;
        }
        m = i; n = j;
        while ( m >= 0 && n < board.length )
        {
            board[m][n] = true;
            m--; n++;
        }
        m = i; n = j;
        for ( int k = j; k < board.length; k++ )
            board[m][k] = true;
        for ( int l = i; l < board.length; l++ )
            board[l][n] = true;
    }
    public String makeString()
    {
        String str = "";
        for ( int i = 0; i < array.length; i++ )
            str += array[i];
        return str;
    }
    public ArrayList<Integer> validMove(int j)
    {
        ArrayList<Integer> validMoves = new ArrayList<Integer>();
        for ( int i = 0; i < board.length; i++ )
        {
            if ( board[i][j] ) validMoves.add(i);
        }
        return validMoves;
    }
    public void printBoard(){
        System.out.println("Latest Board");
         for ( int i = 0; i < board.length; i++ )
        {
            for ( int j = 0; j < board.length; j++ )
            {
                if ( board[i][j] ){
                    System.out.printf("%3s", "QQ" );
                    continue;
                }
                // if ( board[i][j] ) System.out.print(" * ");
                else System.out.print(" _ ");
            }
            System.out.println("");
        }
    }
    public void printHeuristic(){
        System.out.println("Latest Heuristic");
        for ( int i = 0; i < hBoard.length; i++ )
        {
            for ( int j = 0; j < hBoard.length; j++ )
            {
                if ( board[i][j] ) System.out.printf("%3s", " QQ");
                else System.out.printf("%3d", hBoard[i][j] );
            }
            System.out.println("");
        }
    }
    public int heuristic()
    {
        int h = 0, x, y = 0, temp, row, col, size = board.length, round = 0;
        boolean f = false;
        for ( int i = 0; i < board.length; i++ )
        {
            x = 0; y = 0;
            for ( int j = 0; j < board.length; j++ )
            {
                if ( board[i][j] ) x++;
                if ( board[j][i] ) y++;
                // boardI[i][j] += 1;
            }
            h += h_formula(x);
            h += h_formula(y);
        }
        for ( temp = 0; temp < size; temp++ )
        {
            x = 0; row = temp; col = 0;
            while ( row >= 0 && col < size )
            {
                // boardI[row][col] += 1;
                if ( board[row][col] )
                    x++;
                row--; col++;
            }
            h += h_formula(x);
        }
        for ( temp = 1; temp < size; temp++ )
        {
            x = 0; col = temp; row = size-1;
            while ( row >= 0 && col < size )
            {
                // boardI[row][col] += 1;
                if ( board[row][col] )
                    x++;
                row--; col++;
            }
            h += h_formula(x);
        }
        for ( temp = size-1; temp >= 0; temp-- )
        {
            y = 0; row = temp; col = 0;
            while ( row < size && col < size )
            {
                // boardI[row][col] += 1;
                if ( board[row][col] ) y++;
                row++; col++;
            }
            h += h_formula(y);
        }
        for ( temp = 1; temp < size; temp++  )
        {
            y = 0; col = temp; row = 0;
            while ( row < size && col < size )
            {
                // boardI[row][col] += 1;
                if ( board[row][col] ) y++;
                row++; col++;
            }
            h += h_formula(y);
        }
        this.hn = h;
        return h;
    }
    public static int h_formula(int x)
    {
        int h = 0;
        for (int i = 1; i <= x - 1; i++ )
            h += ( x - i );
        return h;
    }
    public void boardCalculation()
    {
        int size = board.length;
        min = Integer.MAX_VALUE;
        for ( int i = 0; i < size; i++ )
        {
            board[array[i]][i] = false;
            for ( int j = 0; j < size; j++ )
            {
                board[j][i] = true;
                hBoard[j][i] = heuristic();
                if ( hBoard[j][i] < min )
                {
                    min = hBoard[j][i];
                    pos[0] = j;
                    pos[1] = i;
                }
                // System.out.printf( "%3d", heuristic());
                board[j][i] = false;
            }
            // System.out.println("");
            board[array[i]][i] = true;
        }
        // System.out.println("MIN : " + min + " i : " + pos[0] + " j : " + pos[1] );
        // state.printBoard();
    }
    public void moves(){
        for ( int i = 0; i < board.length; i++ )
        {
            if ( board[i][pos[1]] ) board[i][pos[1]] = false;
        }
        board[pos[0]][pos[1]] = true;
        array[pos[1]] = pos[0];
    }
    public void randomWalk(int bound, int currentT)
    {
        Random r = new Random();
        if ( r.nextInt(bound) <= currentT )
        {
            pos[0] =  r.nextInt(board.length);
            pos[1] =  r.nextInt(board.length);
            // System.out.println("r");
        }
        moves();
    }
    public int getMin(){ return min; }
    public Object Clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void main(String[] args)
    {
        State state = new State(8);
        state.printBoard();
        int h = state.heuristic();
        System.out.println(">> " + h);
        //state.boardCalculation();
        //state.printHeuristic();
        state.printBoard();
    }
}
