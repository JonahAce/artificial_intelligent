package n_queen;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

public class Game {
    LinkedList<String> completeConfiguration;
    int complexity;
    public Game(int complexity){
        System.out.println("Game Started");
        completeConfiguration = new LinkedList<String>();
        this.complexity = complexity;
    }
    public int SimulatedAnnealing(State state, int boilingPoint, int freezingPoint)
    {
        int status = 1, prev, current = state.heuristic(), counter = 0, temperature = boilingPoint;

        while ( current != 0  ) // while the final configuration is not reached
        {
            state.boardCalculation(); // update heuristic value of each cell
            prev = current; // hold the heuristic value of current state
            if ( state.getMin() < current ) // best heuristic value is lower than current
                state.moves();  // moves to the best state
            else if ( temperature != freezingPoint ) // the freezing point is reached
                // has a chance of taking the randomly generated state at the probability of temperature/bound
                state.randomWalk(boilingPoint, temperature--); // cooling down
            else return -1;
            current = state.heuristic(); // calculate the heuristic value of the current state
        }
        String str = state.makeString(); // generate state-code
        if ( !completeConfiguration.contains(str) ) completeConfiguration.add(str); // store if not previously discovered
        return status; // solved
    }
    public int HillClimbingSideWalk(State state, int allowance) // solve this state and side walk are allowed up to allowance times
    {
        int prev, current = state.heuristic(), counter = 0;
        while ( current != 0 ) // global minimum is zero
        {
            state.boardCalculation(); // calculate the heuristic value of each possible child state
            prev = current; // hold the heuristic value of current state
            state.moves(); // move to the best state
            current = state.heuristic(); // calculate the heuristic value of current state
            while ( prev == current ) // local minimum is detected
            {
                counter++;
                if ( counter == allowance ) return -1; // reach the maximum allowance for side walk
                prev = current;
                state.randomWalk(1,1); // move to a randomly generated state at the probability of nearly 1
                current = state.heuristic(); // calculate the heuristic value of current state
            }
        }
        String str = state.makeString(); // generate state-code
        if ( !completeConfiguration.contains(str) ) completeConfiguration.add(str); // store if not previously discovered
        return 1; // solved
    }
    public void addToPriorityQueue(LinkedList<State> pool, State state) // ASC
    {
        int h = state.heuristic();
        for ( int j = 0; j < pool.size(); j++ )
        {
            if ( pool.get(j).heuristic() > h )
            {
                pool.add(j, state);
                return;
            }
        }
        pool.add(state);
    }
    public LinkedList<State> generateKState(State state, int k)
    {
        LinkedList<State> statePool = new LinkedList<State>();
        for ( int j = 0; j < k; j++ )
        {
            State child = new State(state.array); // construct successor from existing board
            child.randomWalk(1,1); // generate random successor
            // addToPriorityQueue(statePool, child);
            statePool.add(child); // append successor to list
        }
        return statePool;
    }
    public LinkedList<State> statesGenerator(LinkedList<State> statePool )
    {
        LinkedList<State> generator = new LinkedList<State>();
        Random r = new Random();
        for ( int k = 0; k < statePool.size(); k++ )
        {
            // spare the statePool for use later, use get(index) instead of remove()
            State kState = new State( statePool.get(k).array ); // construct successor from existing board
            // System.out.println("PRE : " + kState.heuristic() );
            // reserve size / 9 probability to accept a less optimal ( short term ) action
            if ( r.nextInt(statePool.size()) <= statePool.size() / 9  ) kState.randomWalk(1,1);
            else
            {
                kState.boardCalculation();
                kState.moves();
            }
            // System.out.println("POST : " + kState.heuristic() );
            // addToPriorityQueue(generator, kState);
            generator.add(kState);
        }
        return generator;
    }
    public void selectKBest( LinkedList<State> source, LinkedList<State> destination, int k )
    {
        while ( source.size() != 0 )
        {
            State bus = source.pollFirst();
            if ( bus.heuristic() > destination.get(destination.size()-1).heuristic() ) break; // skip, the cost is too high
            addToPriorityQueue(destination,bus); // ASC
        }
        //System.out.println("Best pool : " + destination.size() );
        while ( destination.size() != k )  destination.pollLast(); // only up to ks successor will be selected
    }
    public LinkedList<State> StochasticBeamSelect(LinkedList<State> source, LinkedList<State> destination, int k)
    {
        LinkedList<State> randomPool = new LinkedList<State>();
        Random r = new Random();
        while ( randomPool.size() != k )
        {
            State A = source.pollFirst();
            State B = destination.pollFirst();
            // Both has probability of 0.5 to be chosen as the successor
            // States will be in ASC as they are added to the list
            if ( r.nextInt(2) == 1 ) addToPriorityQueue(randomPool, A);
            else addToPriorityQueue(randomPool, B);
        }
        return randomPool;
    }
    public int LocalBeamSearch(State state, int k, int depth) // depth is not necessary if allowed to wait
    {
        State shuttle = state;
        if ( state.heuristic() != 0 )
        {
            Random r = new Random(); int counter = 0;
            LinkedList<State> pool = generateKState( state, k ); // generate ks successor from given state

            while ( true )
            {
                LinkedList<State> transit = statesGenerator(pool); // generate successor state for each successor state
                // selectKBest( transit, pool, k );
                pool = StochasticBeamSelect( transit, pool, k ); // select ks random state and store in ASC order
                //System.out.println("current : " + pool.get(0).heuristic() );
                if ( pool.get(0).heuristic() == 0 ) // solved
                {
                    shuttle = pool.pollFirst();
                    break;
                }
                if ( counter++ == depth ) return 0; // reach max-depth
            } // node instruction after this line
        }
        if ( !completeConfiguration.contains(shuttle.makeString()) ) // if not previously discovered
        {
            completeConfiguration.add(shuttle.makeString()); // append to configuration set
            return 1;
        }
        return 0;
    }

    public LinkedList<State> generatePopulation( int complexity, int headCount )
    {
        LinkedList<State> population = new LinkedList<State>();
        while ( population.size() != headCount )
        {
            State state = new State( complexity );
            population.add(state);
        }
        return population;
    }
    public State Reproduce( State Father, State Mother )
    {
        Random r = new Random();
        int []bluePrint =  Father.array.clone();
        int crossoverPoint = r.nextInt(complexity);
        for ( int i = crossoverPoint; i < complexity; i++ )
            bluePrint[i] = Mother.array[i];
        return new State(bluePrint);
    }

    public State RandomSelect(LinkedList<State> population)
    {
        Random r = new Random();
        int total = 0, cumulative = 0, index;
        for ( State state : population )
            total += (28-state.heuristic());
        index = r.nextInt(total);
        // System.out.println("Selecting");
        for ( State state : population )
        {
            cumulative += (28-state.heuristic());
            // System.out.println("h : " + state.heuristic());
            if ( cumulative >= index )
            {
                // System.out.println("\nChosen : " + state.makeString() );
                return new State(state.array);
            }
        }
        return new State(population.get(r.nextInt(population.size()-1)).array);
    }
    public void mutate( State state )
    {   // System.out.println("\nmutating");
        Random r = new Random();
        int section = r.nextInt(state.array.length);
        state.array[section] = r.nextInt(state.array.length);
        // section = r.nextInt(state.array.length);
        // state.array[section] = r.nextInt(state.array.length);
    }
    public int Genetic_Algorithm( LinkedList<State> population )
    {
        Random r = new Random();
        State Father, Mother, Child;
        while ( true )
        {
            LinkedList<State> newPopulation = new LinkedList<State>();
            for ( int i = 0; i < population.size(); i++ )
            {
                Father = RandomSelect(population);
                Mother = RandomSelect(population);
                Child = Reproduce(Father, Mother);
                if ( Child.heuristic() == 0 )
                {
                    if ( !completeConfiguration.contains(Child.makeString()) ) // if not previously discovered
                    {
                        completeConfiguration.add(Child.makeString()); // append to configuration set
                        return 1;
                    }
                    return 0;
                }
                if ( r.nextInt(77) < 7 )
                    mutate(Child);
                addToPriorityQueue(newPopulation, Child);
            }
            population = newPopulation;
        }
    }
    public double[][] experiment()
    {
        double[][] coefV = new double[200-8][2];
        int lowerBound = 8, upperBound = 200, cycle = 100;
        System.out.println("SIM Started");
        for ( int prop = lowerBound; prop < upperBound; prop++ )
        {
            long time =  System.nanoTime();
            for ( int i = 0; i < cycle; i++ )
            {
                Genetic_Algorithm(generatePopulation(complexity,prop));
                completeConfiguration.clear();
            }
            time = System.nanoTime() - time;
            coefV[prop-8][0] = prop;
            coefV[prop-8][1] = (double)time/1000000/1000/cycle;
            System.out.println("Prop : " + prop + ", Time : " + coefV[prop-8][1] );
        }
        System.out.println("SIM Ended");
        for ( int i = 0; i < upperBound-lowerBound; i++ )
        {
            double bar = coefV[i][1];
            System.out.print("prop : " + coefV[i][0] + ", time :" + coefV[i][1] + "\n" );
            if ( bar > 50 )
            {
                System.out.println(">> deduct 50");
                bar -= 50;
            }
            while ( bar > 0 )
            {
                System.out.print("*");
                bar -= 0.2;
            }
            System.out.println("\n");
        }
        return coefV;
    }

    public static void main(String[] args)
    {
        Game game = new Game(8);
        // game.experiment();
        int moves = 0;
        double strike = 0;
        long time =  System.nanoTime();
        while ( game.completeConfiguration.size() != 1 )
        {
            State state = new State(game.complexity);
            // int status = game.SimulatedAnnealing(state, 100, 0);
            // int status = game.HillClimbingSideWalk(state, 17);
            // int status = game.LocalBeamSearch( state,7, Integer.MAX_VALUE );
            int status = game.Genetic_Algorithm( game.generatePopulation(game.complexity, 30 ) );
            if ( status == 1 ) strike++;
            moves++;
        }
        time = System.nanoTime() - time;
        System.out.println("Elapsed Time : " + ((double)time/1000000/1000));
        System.out.println("Moves : " + moves );
        System.out.println("Rate : " + (strike/moves) );
        game.completeConfiguration.forEach(item->{
            System.out.println(">> " + item );
        });
    }
}
