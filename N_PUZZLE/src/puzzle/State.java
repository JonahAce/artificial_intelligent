package puzzle;

import java.util.LinkedList;

public class State {
    public int board[][];
    public int fn;
    public int gn;
    public State parent; // trace the parent state through this variable
    public State(int size)
    {
        board = new int[size][size];
        int i, j; i = j = 0;
        for ( ; i < size; i++ )
        {
            for ( j = 0; j < size; j++ )
                board[i][j] = i * size + j + 1;
        }
        board[size-1][size-1] = 0;
    }
    public State(int givenState[][])
    {
        board = State.deepCopyIntMatrix(givenState);
        gn = 0;
        fn = gn + State.heuristic(board, Game.goalState.board); // gn + hn
    }
    public static State stateGenerator( int[][] board, int[] oldPos, int[] newPos, int gn )
    {
        State childState = new State(board);
        childState.gn = gn;
        childState.swap(oldPos[0], oldPos[1], newPos[0], newPos[1]);
        int hn = State.heuristic(childState.board, Game.goalState.board);
        childState.fn = childState.gn + hn;
        return childState;
    }
    public void printBoard()
    {
        System.out.println(">> Latest Board");
        for ( int i = 0; i < board.length; i++ )
        {
            for ( int j = 0; j < board.length; j++ )
            {
                if ( j == 0 )
                    System.out.print(" |");
                System.out.printf("%s%2d%s", " ", board[i][j], " ");
            }
            System.out.println(" |");
        }
        System.out.println("");
    }
    public String getHashKey()
    {
        String str = "";
        for ( int i = 0; i < board.length; i++ )
        {
            for ( int j = 0; j < board.length; j++ )
                str += board[i][j];
        }
        return str;
    }
    public static int[][] deepCopyIntMatrix(int[][] input) {
        if (input == null) return null;
        int[][] result = new int[input.length][];
        for (int r = 0; r < input.length; r++)
            result[r] = input[r].clone();
        return result;
    }
    public static int max(int v1, int v2)
    {
        if ( v1 > v2 ) return v1;
        else if ( v1 < v2 ) return v2;
        return v1;
    }

    public static int heuristic(int[][] current, int[][] goal )
    {
        int h1 = 0, h2 = 0, lc = linearConflict(current, goal);
        for ( int i = 0; i < current.length; i++ )
        {
            for ( int j = 0; j < current.length; j++ )
            {
                if ( current[i][j] != goal[i][j] )
                {
                    h1++;
                    if ( current[i][j] == 0 ) current[i][j] = current.length * current.length;
                    int homerow = ( current[i][j] - 1 ) / current.length;
                    int homecol = ( current[i][j] - 1 ) % current.length;
                    h2 += Math.abs(homerow - i) + Math.abs(homecol - j);
                    if ( current[i][j] == current.length * current.length ) current[i][j] = 0;
                }
            }
        }
        //System.out.println("linear conflict : " + lc );
        h2 += ( 2 * lc );
        return State.max(h1,h2);
    }

public static int linearConflict(int[][] current, int[][] goal )
    {
        int conflictV = 0;
        int[] rP = new int[current.length * current.length+1];
        int[] cP = new int[current.length * current.length+1];
        for ( int i = 0; i < current.length; i++ )
        {
            for ( int j = 0; j < current.length; j++ )
            {
                // System.out.println("i : " + i + ", j : " + j + ", goal : " + goal[i][j] );
                rP[goal[i][j]] = i;
                cP[goal[i][j]] = j;
            }
        }
        // row conflicts
        for ( int row = 0; row < current.length; row++ )
        {
            for ( int left = 0; left < current.length; left++ )
            {
                for ( int right = left + 1; right < current.length; right++ )
                {
                    if ( current[row][left] != 0 && current[row][right] != 0 && rP[current[row][left]] == row && rP[current[row][right]] == row &&cP[current[row][left]] > cP[current[row][right]])
                    {
                        conflictV++;
                        // System.out.println( "row, left, right : " + row + ", " + left + ", " + right );
                    }
                }
            }
        }
        // System.out.println( "conflict count : " + conflictV );
        // col conflicts
        for ( int col = 0; col < current.length; col++ )
        {
            for ( int up = 0; up < current.length; up++ )
            {
                for ( int down = up + 1; down < current.length; down++ )
                {
                    if ( current[up][col] != 0 && current[down][col] != 0 && cP[current[up][col]] == col && cP[current[down][col]] == col && rP[current[up][col]] > rP[current[down][col]] )
                    {
                        conflictV++;
                        // System.out.println( "col, up, down : " + col + ", " + up + ", " + down );
                    }
                }
            }
        }
        // System.out.println( "Conflict count : " + conflictV );
        return conflictV;
    }

    public boolean isSolvable()
    {
        int inv_count = 0, zero = 0;
        for ( int i = 0; i < board.length * board.length; i++ )
        {
            if ( board[i/board.length][i%board.length] == 0 ) continue;
            for ( int j = i+1; j < board.length*board.length; j++ )
            {
                if ( board[j/board.length][j%board.length] == 0 ) board[j/board.length][j%board.length] = board.length*board.length;
                if ( board[i/board.length][i%board.length] > board[j/board.length][j%board.length] ) inv_count++;
                if ( board[j/board.length][j%board.length] == board.length*board.length ) board[j/board.length][j%board.length] = 0;
            }
        }
        int pos[] = this.getZeroPosition();
        zero = pos[0]; // * board.length + pos[1];
        System.out.println("inv : " + inv_count);
        System.out.println("zero : " + zero);
        if ( board.length % 2 == 0 )
        {
            if ( zero % 2 ==  1 )
                return !( inv_count%2 == 1 );
            else
                return inv_count % 2 == 1;
        }
        else
        {
            if ( inv_count % 2 == 0 ) return true;
            else return false;
        }
    }
    public int[] getZeroPosition()
    {
        for ( int i = 0; i < board.length; i++ )
        {
            for ( int j = 0; j < board.length; j++ )
            {
                if ( board[i][j] == 0 )
                    return new int[]{i, j};
            }
        }
        return null; // not possible
    }
    public void swap(int i1, int j1, int i2, int j2)
    {
        board[i1][j1] = board[i1][j1] + board[i2][j2];
        board[i2][j2] = board[i1][j1] - board[i2][j2];
        board[i1][j1] = board[i1][j1] - board[i2][j2];
    }
    public static boolean validMove(int i, int j, int max)
    {
        if ( i < 0 || i >= max ) return false;
        if ( j < 0 || j >= max ) return false;
        return true;
    }
    public void stepPath(State initialState)
    {
        LinkedList<State> path = new LinkedList<State>();
        State current = this;
        while ( current != initialState )
        {
            path.push(current);
            current = current.parent;
        }
        path.forEach(item->{
            item.printBoard();
        });
    }
}
