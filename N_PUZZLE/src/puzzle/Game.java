package puzzle;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

public class Game
{
    LinkedList<State> priorityQueue;
    HashMap<String, State> exploredMap;
    public static State goalState = new State(4);
    public Game(int size)
    {
        priorityQueue = new LinkedList<State>();
        exploredMap = new HashMap<String, State>();
    }
    public static boolean isIdentical(int oldState[][], int newState[][])
    {
        int counter = 0;
        for ( int i = 0; i < oldState.length; i++ )
        {
            for ( int j = 0; j < oldState.length; j++ )
            {
                if ( oldState[i][j] != newState[i][j] )
                    return false;
            }
        }
        return true;
    }
    public static void insertChildState(LinkedList<State> list, HashMap<String, State> exploredMap, State givenState)
    {
        givenState.fn = givenState.gn + State.heuristic(givenState.board, new State(givenState.board.length).board);
        int index = 0;
        //if ( exploredMap.containsKey(givenState.getHashKey())) // check else where
        //    return;
        for ( State state : list )
        {
            if ( isIdentical( state.board, givenState.board ) ) return;
            if ( givenState.fn <= state.fn )
            {
                list.add(index,givenState);
                return;
            }
            else index++;
        }
        list.add(givenState);
    }
    public int A_Star(int givenState[][])
    {
        State state = new State(givenState);
        if ( !state.isSolvable() ) return Integer.MAX_VALUE;
        System.out.println(">> SOLVABLE\n");
        priorityQueue.add(state);
        while ( !priorityQueue.isEmpty() )
        {
            State current = priorityQueue.pollFirst();
            if ( Game.isIdentical(current.board, Game.goalState.board) )
            {
                current.stepPath(state);
                System.out.println(">> Explored " +  exploredMap.size() + " board");
                System.out.println(">> Queued : " + priorityQueue.size() );
                return current.gn;
            }
            if ( exploredMap.containsKey(current.getHashKey())) continue;
            int[] zeroPos = current.getZeroPosition();
            LinkedList<int[]> validMoves = Game.getValidMoves(current);
            while ( !validMoves.isEmpty() )
            {
                int position[] = validMoves.pollLast();
                State childState = State.stateGenerator(current.board, zeroPos, position, current.gn + 1);
                childState.parent = current;
                Game.insertChildState(priorityQueue, exploredMap, childState);
            }
        }
        System.out.println(">> FAILED");
        return Integer.MAX_VALUE;
    }

    public static LinkedList<int[]> getValidMoves(State state)
    {
        int position[] = state.getZeroPosition();
        int size = state.board.length;
        LinkedList<int[]> validMoves = new LinkedList<int[]>();
        if ( State.validMove(position[0] + 1, position[1], size ) ) validMoves.add( new int[]{ position[0] + 1, position[1] } );
        if ( State.validMove(position[0] - 1, position[1], size ) ) validMoves.add( new int[]{ position[0]-1, position[1] } );
        if ( State.validMove(position[0], position[1] + 1, size ) ) validMoves.add( new int[]{ position[0], position[1] + 1 } );
        if ( State.validMove(position[0], position[1] - 1, size ) ) validMoves.add( new int[]{ position[0], position[1] - 1 } );
        return validMoves;
    }
    public static State messGenerator(int size)
    {
        State state = new State(size);
        int i = 0;
        Random r = new Random();
        LinkedList<int[]> validMoves = Game.getValidMoves(state);
        while ( !validMoves.isEmpty() && i < 10 )
        {
            int index = r.nextInt(validMoves.size());
            int move[] = validMoves.get(index);
            state.swap(state.getZeroPosition()[0], state.getZeroPosition()[1], move[0], move[1]);
            i += 1;
            validMoves = Game.getValidMoves(state);
        }
        return state;
    }
    public static void messier(State state)
    {
        Random r = new Random();
        int move[] = new int[2];
        move[0] = r.nextInt(state.board.length);
        move[1] = r.nextInt(state.board.length);
        state.swap(state.getZeroPosition()[0], state.getZeroPosition()[1], move[0], move[1]);
    }
    public static void main(String[] args)
    {
        int size = 4;
        int[][] givenState = {
                {6,13,7,10},
                {8,9,11,0},
                {15,2,12,5},
                {14,3,1,4}
        };
        Game game = new Game(size);
        State mess = Game.messGenerator(size);
        mess.printBoard();
        //Game.messier(mess);
        //System.out.println("Messier");
        //mess.printBoard();
        long startTime = System.nanoTime();
        int moves = game.A_Star(mess.board);
        // int moves = game.A_Star(givenState);
        long estimatedTime = System.nanoTime() - startTime;
        System.out.println("MOVES : " + moves);
        System.out.println("Elapsed time : " + estimatedTime/3600000);
    }
}
