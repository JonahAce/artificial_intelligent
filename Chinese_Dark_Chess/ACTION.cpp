#include "ACTION.h"

Action::Action()
{
	memset( dir, 0, 2 * sizeof(int) );
}

Action::Action( const Action & a )
{
	setAction( a.dir );
}

Action::~Action( void ) { }

void Action::setReward( int r ){ reward = r; }

void Action::setAction( const int mov[2] )
{
	dir[0] = mov[0];
	dir[1] = mov[1];
}

int Action::getReward() { return reward; }

void Action::getAction( int moves[2] )
{
	moves[0] = dir[0];
	moves[1] = dir[1];
}


