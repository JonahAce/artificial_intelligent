#ifndef ACTION_INCLUDED
#define ACTION_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <list>

class Action{
	public:
		Action();
		Action( const Action & a );
		~Action( void );
		void setReward( int reward );
		void setAction( const int mov[2] );
		int getReward();
		void getAction( int moves[2] );
		int reward;
	private:
		int dir[2];
};

#endif


