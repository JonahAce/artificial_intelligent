#define ROW 20
#define COL 6

int readFile(char *path, double dataframe[ROW][COL])
{
	FILE *fptr;
	double var;
	int i = 0, j = 0;
	if ( (fptr = fopen(path, "r")) == NULL )
	{
		printf(">> IO Exception : File not found\n");
		return 0;
	}
	puts(">> File Opened\n");
	while ( fscanf(fptr, "%lf", &var) != EOF )
	{
		// printf("%lf ", var);
		dataframe[i][j] = var;
		j++;
		if ( j == COL )
		{
			i++;
			j = 0;
			// puts("");
		}
	}
	fclose(fptr);
	return i;
}

int writeFile( char *path, double dataframe[], int row, char *mode)
{
	FILE *fptr;
	int i = 0, j = 0;
	if ( (fptr = fopen(path, mode)) == NULL )
	{
		printf(">> IO Exception : File cannot open\n");
		return 0;
	}
	for ( i = 0; i < row; i++ )
	{
		fprintf(fptr, "%d\t%14.6lf\n", count++, dataframe[i]);
	}
	fclose(fptr);
	return 1;
}
