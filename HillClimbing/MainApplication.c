#include <stdio.h>
#include <stdlib.h>
#include <time.h>
// constant variables
#define DIM 6
#define ROW 20
#define COL 6
#define STEP 0.05
#define DEPTH 10000
#define RFACTOR 100
#define PFACTOR 7
#define FGOAL 0.95 
// file pathway
#define DATA "data.txt"
#define PROGRESS "progress.txt"
#define OUTPUT "output.txt"
// global variables
int count = 0;
// self define library
#include "RF.h"
#include "multiR.h"
// function prototype
void printDataframe(double dataframe[ROW][COL]);
void printCVector(double cVector[COL]);
void copyArray(double src[], double dest[]);
void output(double dataframe[ROW][COL], double cVector[COL]);
double getLinearExpression(double dataframe[ROW][COL], double cVector[COL], double mean, char *progress);
double randomStochastic(double dataframe[ROW][COL], double cVector[COL], double mean, char *progress);
// main program
int main(void)
{	
	srand(time(NULL));
	// variable segment
	double dataframe[ROW][COL] = {0};
	double cVector[COL], cV[COL];
	int j;
	clock_t t;
	double elapsedTime, min, miu;
	// initialize
	for ( j = 0; j < COL; j++ ) cVector[j] = ( (rand() % RFACTOR) * ((rand() % 2 == 1)?1:-1) ) + (double)rand()/(double)(RAND_MAX/3);
	// program segment
	readFile(DATA, dataframe); // printDataframe(dataframe);
	miu = mean(dataframe);
 
	// Random Stochastic Hill Climbing
	t = clock();
	min = randomStochastic(dataframe, cVector, miu, PROGRESS);
	t = clock() - t;
	elapsedTime = ((double)t)/CLOCKS_PER_SEC;
	printf( ">> Elapsed Time : %8.6lf\n", elapsedTime );
	printCVector(cVector);
	printf("%15.6lf\n", min);

	// Goal Orientated Hill Climbing
	t = clock();
	min = getLinearExpression(dataframe, cVector, miu, PROGRESS);
	t = clock() - t;
	elapsedTime = ((double)t)/CLOCKS_PER_SEC;
	printf( ">> Elapsed Time : %8.6lf\n", elapsedTime );
	printCVector(cVector);

	output(dataframe, cVector);
	return 0;
}

void printDataframe(double dataframe[ROW][COL])
{
	int i,j;
	for ( i = 0; i < ROW; i++ )
	{	printf(">> %2d ", i);
		for ( j = 0; j < COL; j++ )	printf("%15.6lf ", dataframe[i][j]);
		puts("");	
	}
}

void printCVector(double cVector[COL])
{
	int j;
	for ( j = 0; j < COL; j++ ) printf("%2.6lf\t", cVector[j]);	
}
void copyArray(double src[], double dest[])
{
	int i;
	for ( i = 0; i < COL; i++ )	dest[i] = src[i];
}

double getLinearExpression(double dataframe[ROW][COL], double cVector[COL], double mean, char *progress)
{
	printf("\n\nGoal-orientated Hill-Climbing\n");
	double prev, current = R2(dataframe, mean, cVector), fitness = current;
	double tVector[COL] = {0};
	int iteration = 0, index, dir, probe;
	// save a copy of current state
	copyArray(cVector, tVector);
	while ( fitness < FGOAL+0.025 ) // && depth <= DEPTH
	{
		prev = current; // track fitness of before and after for later comparison
		index = rand() % COL; // decide which coef to be adjusted
		dir = ( rand() % 2 == 1 ) ? 1 : -1; // decide whether to increase or decrease
		cVector[index] += ( dir * STEP ); // update selected coef
		current = R2(dataframe, mean, cVector); // recalculate fitness
		if ( current > fitness )
		{
			fitness = current; // update fitness level
			copyArray(cVector, tVector); // update best fit coef vectors
		}
		probe = PFACTOR; // reset probe, start with a wider gap
		while ( current > prev ) // we are in the rigth path of raising the fitness
		{
			probe *= 0.8; // incremental speed decrease over time so that we don't miss the extrema
			prev = current; // track fitness of before and after for later comparison
			cVector[index] += ( dir * STEP * probe ); // update selected coef
			current = R2(dataframe, mean, cVector); // recalculate fitness
			if ( current > fitness )
			{
				fitness = current; // update fitness level
				copyArray(cVector, tVector); // update best fit coef vectors
			}
			iteration++;
		}
		iteration++;
	}
	printf("\n>> Iteration : %d\n", iteration);
	printf(">> Fitness : %15.6lf\n", fitness );
	copyArray(tVector, cVector); // update best fit coef vectors
	return fitness;
}

double randomStochastic(double dataframe[ROW][COL], double cVector[COL], double mean, char *progress)
{
	printf("Random Stochastic Hill-Climbing\n");
	double prev, current = R2(dataframe, mean, cVector), fitness = current;
	double tVector[COL] = {0};
	int iteration = 0, j;
	// save a copy of current state
	copyArray(cVector, tVector);
	while ( current < FGOAL ) // && depth <= DEPTH  
	{
		// generate another set of random coef vector
		for ( j = 0; j < COL; j++ ) cVector[j] = ( (rand() % RFACTOR) * (rand() % 2 == 1)?1:-1 ) + (double)rand()/(double)(RAND_MAX/3);
		current = R2(dataframe, mean, cVector); // recalculate fitness
		if ( current > fitness )
		{
			fitness = current; // update fitness level
			copyArray(cVector, tVector); // update best fit coef vectors
		}
		iteration++;
	}
	printf("\n>> Iteration : %d\n", iteration);
	printf(">> Fitness : %15.6lf\n", fitness );
	// copyArray(tVector, cVector); // update best fit coef vectors
	return fitness;
}

void output(double dataframe[ROW][COL], double cVector[COL])
{
	int i, j; double estimatedY, total, miu = mean(dataframe), reg, var, fitness, v = 0, diff;
	printf("%15.6lf\n", miu);
	printf("\n%15s\t%15s\t%15s\n", "ACTUAL", "ESTIMATED_Y", "DIFFERENCE");
	for ( i = 0; i < ROW; i++ )
	{
		estimatedY = getY(dataframe[i], cVector);
		diff = dataframe[i][5] - estimatedY;
		v += abs(diff);
		printf("%15.6lf\t%15.6lf\t%15.6lf\n", dataframe[i][5], estimatedY, diff);
	} puts("");
	reg = SSReg(dataframe,miu,cVector);
	total = SSTot(dataframe, miu, cVector);
	var = getRS(dataframe,cVector);
	fitness = R2(dataframe, miu, cVector);
	printf(">>    SSReg : %15.6lf\n", reg);
	printf(">>    SSTot : %15.6lf\n", total);
	printf(">> E_Square : %15.6lf\n", var);
	printf(">>  Fitness : %15.6lf\n", fitness);
	printf(">> Tot : %15.6lf\n", v/20 );
}


