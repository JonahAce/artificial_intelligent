// constant variables
#define ROW 20
#define COL 6

double mean(double dataframe[ROW][COL])
{
	int i; double total = 0, mean;
	for ( i = 0; i < ROW; i++ )
	{
		total += dataframe[i][5];
	}
	mean = total / (double)ROW;
	return mean;
}

double getY(double xVector[COL], double cVector[COL])
{
	int i, j; double y = cVector[0];
	for ( j = 1; j < COL; j++ )
	{
		y += xVector[j-1] * cVector[j];
	}
	return y;
}

double getRS(double dataframe[ROW][COL], double cVector[COL])
{
	int i, j; double RS = 0, diff;
	for ( i = 0; i < ROW; i++ )
	{
		diff = dataframe[i][5] - getY(dataframe[i], cVector);
		RS += ( diff * diff );
	}
	return RS;
}

double SSReg(double dataframe[ROW][COL], double mean, double cVector[COL])
{
	double reg = 0, fi, diff, tot = 0;
	int i;
	for ( i = 0; i < ROW; i++ )
	{
		fi = getY(dataframe[i], cVector);
		diff = ( fi - mean );
		reg += ( diff * diff );
	}
	return reg;
}

double SSTot(double dataframe[ROW][COL], double mean, double cVector[COL])
{
	double reg = 0, yi, diff, tot = 0;
	int i;
	for ( i = 0; i < ROW; i++ )
	{
		diff = dataframe[i][5] - mean;
		tot += ( diff * diff );
	}
	return tot;
}

double R2(double dataframe[ROW][COL], double mean, double cVector[COL])
{
	double R2 = 1 - ( getRS(dataframe, cVector) / SSTot(dataframe, mean, cVector) );
	return R2;
}
